package bcas.ap.inter;

public class Toyota implements Car{

	@Override
	public String getBody() {
		
		return "corolla";
	}

	@Override
	public String getColour() {
		
		return "gray";
	}

	@Override
	public int getAnnualCost() {
		
		return 2245;
	}


}
