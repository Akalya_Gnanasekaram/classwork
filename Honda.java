package bcas.ap.inter;

public class Honda implements Car {

	@Override
	public String getBody() {

		return "Accord LX";
	}

	@Override
	public String getColour() {

		return "black";
	}

	@Override
	public int getAnnualCost() {

		return 2374;
	}

}
